#include "WyrazenieZesp.hh"
#include <iostream>
#include <cstring>
#include <cassert>
#include <stdlib.h>
#include <math.h>

/*
 * Wpisz liczbe jest wpisany po to:
 *                                 -aby usprawnić Wpisz_Wyrazenie
 *                                 -aby później przyjmować wynik 
 * Funkcja ma za zadanie odczytac podana licze zespolona zlozona
 * z czesci rzeczywistej i urojonej, a nasteonie zapamietac ja jako calosc 
 *                                                            
 */

LZespolona Wpisz_Liczbe()
{
  LZespolona tmp;
  int pomocnicza;
  std::cout<<"Część rzeczywista: ";
  std::cin>>pomocnicza;
  tmp.re=pomocnicza;
  pomocnicza=0;
   std::cout<<"Część urojona: ";
  std::cin>>pomocnicza;
  tmp.im=pomocnicza;
  pomocnicza=0;
  return tmp;
}

/*
 * Jak wszystko pójdzie zgodnie z planem funkcja zapisuje podane wartości:
 *    -Arg1 
 *    -Op (zmienna od znaku)
 *    -Arg2 
 * jako cale wyrazenie,wspomaga sie funkcja Wpisz_Liczbe
 *    Zwraca : WyrazenieZesp 
 * Jest stworzona po to aby pozniej łatwiej bylo je obliczac, wypisywać i 
 * przenosic
 */

WyrazenieZesp  Wpisz_Wyrazenie()
{
  char znak;
  LZespolona Liczba,Liczba2;
  WyrazenieZesp WyrZ;
  std::cout<<"Wpisz pierwszą liczbe zespoloną:\n";
  Liczba = Wpisz_Liczbe();
  WyrZ.Arg1.re=Liczba.re;
  WyrZ.Arg1.im=Liczba.im;
  std::cout<<"Wpisz znak:\n";
  std::cin>>znak;
   switch(znak)
    {
    case '+':
      {
	WyrZ.Op=Op_Dodaj;
	break;
      }
    case '-':
      {
	WyrZ.Op=Op_Odejmij;
	break;
      }
    case '*':
      {
	WyrZ.Op=Op_Mnoz;
	break;
      }
    case '/':
      {
	WyrZ.Op=Op_Dziel;
	break;
      }
    default :
      {
	std::cout<<"Bląd nie znam takiego znaku\n";
	abort();
	break;
      }
    }
  std::cout<<"Wpisz drugą liczbe zespoloną:\n";
  Liczba2 = Wpisz_Liczbe();
  WyrZ.Arg2.re=Liczba2.re;
  WyrZ.Arg2.im=Liczba2.im;
  return WyrZ;
  
}

/* 
 * Funkcja ma za zadanie po kolei wyswietlic:
 *     -na poczatek znak "(" 
 *     -nastepnie czesc zeczywista liczby (Liczba.re)
 *     -nastepnie odpowiedzni znak (-/+) miedzy cyframi
 *     -nastepnie czesc urojona liczby (Liczba.im) 
 *     -na koniec "i" i znak ")"
 *
 * Funkcja jest stworzona po to aby łatwo bylo wyswietli JEDNA liczbe zespolona
 *                                         
 */


void Wyswietl_Wynik(LZespolona Liczba)
{
  std::cout<<"(";
  std::cout<<Liczba.re;
  if(Liczba.im>=0)
    {
      std::cout<<"+";
    }
  std::cout<<Liczba.im;
  std::cout<<"i)"; 
}

/*
 * Funkcja  Wyswietl ma za zadanie wyswietlic  caaaaaaaaaaaaaaaałe wyrazenie,
 * a nie tylko jedna liczbe zespolona Wyswietla na poczatku 
 *             -Arg1 (na tej samej zasadzie co funkcja Wyswietl_Wynik)
 *             -nastepnie wyswietla odpowiedni znak
 *             -na koniec Arg 2(rowniez jak funkcja Wyswietl_Wynik)
 *                                           
 */

void Wyswietl (WyrazenieZesp WyrZ)
{
  std::cout<<"(";
  std::cout<<WyrZ.Arg1.re;
  if(WyrZ.Arg1.im>=0)
    {
      std::cout<<"+";
    }
  std::cout<<WyrZ.Arg1.im;
  std::cout<<"i)";
  switch(WyrZ.Op)
    {
    case Op_Dodaj:
      {
	std::cout<<"+";
	break;
      }
    case Op_Odejmij:
      {
	std::cout<<"-";
	break;
      }
    case Op_Mnoz:
      {
	std::cout<<"*";
	break;
      }
    case Op_Dziel:
      {
	std::cout<<"/";
	break;
      }
    }
  std::cout<<"(";
  std::cout<<WyrZ.Arg2.re;
  if(WyrZ.Arg2.im>=0)
    {
      std::cout<<"+";
    }
  std::cout<<WyrZ.Arg2.im;
  std::cout<<"i)";
}


/* Mądrala przelicza cale  wyrazenie odpowiednio do znaku ("+"/"-"/"*"/"/")
 * z zastrzezeniem ze jesli dzielimy przez 0 to program pokazuje blad oraz
 * zatrzymuje program. 
 * Gdy odpowiednie dzialania sa wykonane program:
 *     zwraca Wynik zapisany jako calosc zespolona:
 *                                                -czesc rzeczywista (Wynik.re) 
 *                                                -czesc urojona (Wynik.im)
 *                                                    
 */

LZespolona Oblicz(WyrazenieZesp WyrZ)
{
  LZespolona Wynik;
  
  switch(WyrZ.Op)
    {
     case Op_Dodaj:
      {
	Wynik.re=WyrZ.Arg1.re+WyrZ.Arg2.re;
	Wynik.im=WyrZ.Arg1.im+WyrZ.Arg2.im;
	return Wynik;
	break;
      }
    case Op_Odejmij:
      {
        Wynik.re=WyrZ.Arg1.re-WyrZ.Arg2.re;
	Wynik.im=WyrZ.Arg1.im-WyrZ.Arg2.im;
	return Wynik;
	break;
      }
    case Op_Mnoz:
      {
	Wynik.re=(WyrZ.Arg1.re*WyrZ.Arg2.re)-(WyrZ.Arg1.im*WyrZ.Arg2.im);
	Wynik.im=(WyrZ.Arg1.re*WyrZ.Arg2.im)+(WyrZ.Arg1.im*WyrZ.Arg2.re);
	return Wynik;
	break;
      }
    case Op_Dziel:
      {
	double pomocnicze;
	pomocnicze=((WyrZ.Arg2.re*WyrZ.Arg2.re)+(WyrZ.Arg2.im*WyrZ.Arg2.im));
	if(pomocnicze==0)
	  {
	    std::cout<<"Błąd, dzielisz przez zero\n";
	    abort();
	    break;
	  }
	Wynik.re=((WyrZ.Arg1.re*WyrZ.Arg2.re)-(WyrZ.Arg1.im*(-WyrZ.Arg2.im)))/pomocnicze;
	Wynik.im=((WyrZ.Arg1.re*(-WyrZ.Arg2.im))+(WyrZ.Arg1.im*WyrZ.Arg2.re))/pomocnicze;
	return Wynik;
	break;
      }
    }
}
